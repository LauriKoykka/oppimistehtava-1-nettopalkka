<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Lauri Köykkä">
    <title>Document</title>
</head>
<body>
    <h3>Nettopalkka</h3>
    <?php
    $brutto = filter_input(INPUT_POST, "brutto", FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $ennakko = filter_input(INPUT_POST, "ennakko", FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $elake = filter_input(INPUT_POST, "elake", FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $tyoton = filter_input(INPUT_POST, "tyoton", FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    
    $ennakkopidatys = $brutto * ($ennakko / 100);
    $elakemaksu = $brutto * ($elake / 100);
    $tyotonmaksu = $brutto * ($tyoton / 100);
    $netto = $brutto - ((($brutto / 100) * $ennakko) + (($brutto / 100) * $elake) + (($brutto / 100) * $tyoton));
    
    printf("<p>Ennakkopidätys on %.2f €</p>", $ennakkopidatys);
    printf("<p>Työeläkemaksu on %.2f €</p>", $elakemaksu);
    printf("<p>Työttömyysvakuutusmaksu on %.2f €</p>", $tyotonmaksu);
    printf("<p>Nettopalkka on %.2f €</p>", $netto);
    ?>
    <a href="index.html">Laske uudelleen</a>
</body>
</html>